namespace Zenject.SpaceFighter
{
    public struct PlayerZeroHealthSignal { }
    public struct PlayerDiedSignal { }
    public struct PlayerRelivedSignal { }
    public struct EnemyKilledSignal { }
    public struct GameResumedSignal { }
    public struct GameEndedSignal { }
}
