using System;
using UnityEngine;

namespace Zenject.SpaceFighter
{
    public class PlayerHealthWatcher : ITickable
    {
        readonly SignalBus _signalBus;
        readonly Player _player;

        public PlayerHealthWatcher(
            Player player,
            SignalBus signalBus)
        {
            _signalBus = signalBus;
            _player = player;
        }

        public void Tick()
        {
            if (_player.Health <= 0 && !_player.IsDead)
            {
               _signalBus.Fire<PlayerZeroHealthSignal>();
            }
        }

    }
}
