using System;
using UnityEngine;

namespace Zenject.SpaceFighter
{
    public class PlayerShootHandler : ITickable, IInitializable, IDisposable
    {
        readonly SignalBus _signalBus;
        readonly PlayerInputState _inputState;
        readonly Shooter _shooter;
        readonly Player _player;

        public PlayerShootHandler(
            SignalBus signalBus,
            PlayerInputState inputState,
            Player player,
            Shooter shooter)
        {
            _signalBus = signalBus;
            _player = player;
            _inputState = inputState;
            _shooter = shooter;

        }
        public void Initialize()
        {
            _signalBus.Subscribe<PlayerDiedSignal>(OnPlayerDied);
        }
        public void Dispose()
        {
            _signalBus.Unsubscribe<PlayerDiedSignal>(OnPlayerDied);
        }

        public void Tick()
        {
            if (_player.IsDead)
            {
                return;
            }
            if (_inputState.IsFiring && _shooter.CanShoot)
            {
                _shooter.Fire();
            }
        }

        private void OnPlayerDied() 
        {
            _shooter.StopFire();
        }

    }
}
