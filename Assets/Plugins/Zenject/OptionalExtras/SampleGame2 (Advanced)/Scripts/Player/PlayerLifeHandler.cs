﻿using System;
using UnityEngine;

namespace Zenject.SpaceFighter
{
    public class PlayerLifeHandler : IInitializable, IDisposable
    {
        readonly SignalBus _signalBus;
        readonly AudioPlayer _audioPlayer;
        readonly Settings _settings;
        readonly Explosion.Factory _explosionFactory;
        readonly Player _player;

        public PlayerLifeHandler(
            Player player,
            Explosion.Factory explosionFactory,
            Settings settings,
            AudioPlayer audioPlayer,
            SignalBus signalBus)
        {
            _signalBus = signalBus;
            _audioPlayer = audioPlayer;
            _settings = settings;
            _explosionFactory = explosionFactory;
            _player = player;
        }

        public void Initialize()
        {
            _signalBus.Subscribe<PlayerZeroHealthSignal>(OnPlayerZeroHealth);
            _signalBus.Subscribe<GameResumedSignal>(OnGameResume);
        }
        public void Dispose()
        {
            _signalBus.Unsubscribe<PlayerZeroHealthSignal>(OnPlayerZeroHealth);
            _signalBus.Unsubscribe<GameResumedSignal>(OnGameResume);
        }
        private void OnGameResume()
        {
            Relive();
        }
        private void OnPlayerZeroHealth() 
        {
            Die();
        }

        private void Relive()
        {
            _player.IsDead = false;
            _player.Renderer.enabled = true;
            _player.RestoreHealth();
            _signalBus.Fire<PlayerRelivedSignal>();
        }

        private void Die()
        {
            _player.IsDead = true;
            var explosion = _explosionFactory.Create();
            explosion.transform.position = _player.Position;
            _player.Renderer.enabled = false;
            _signalBus.Fire<PlayerDiedSignal>();
            _audioPlayer.Play(_settings.DeathSound, _settings.DeathSoundVolume);
        }



        [Serializable]
        public class Settings
        {
            public AudioClip DeathSound;
            public float DeathSoundVolume = 1.0f;
        }
    }
}
