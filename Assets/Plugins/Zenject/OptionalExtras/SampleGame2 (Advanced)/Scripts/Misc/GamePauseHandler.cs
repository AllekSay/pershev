﻿using System;
using UnityEngine;

namespace Zenject.SpaceFighter
{
    public class GamePauseHandler : IInitializable, IDisposable
    {
        readonly SignalBus _signalBus;
        public GamePauseHandler(SignalBus signalBus) 
        {
            _signalBus = signalBus;
        }

        public void Initialize()
        {
            _signalBus.Subscribe<PlayerDiedSignal>(OnPlayerDied);
            _signalBus.Subscribe<GameResumedSignal>(OnGameResume);
            _signalBus.Subscribe<GameEndedSignal>(OnGameResume);
        }

        public void Dispose()
        {
            _signalBus.Unsubscribe<PlayerDiedSignal>(OnPlayerDied);
            _signalBus.Unsubscribe<GameResumedSignal>(OnGameResume);
            _signalBus.Subscribe<GameEndedSignal>(OnGameEnded);
        }
        private void OnPlayerDied() 
        {
            PauseGame();
        }
        private void OnGameResume()
        {
            UnPauseGame();
        }
        private void OnGameEnded()
        {
            UnPauseGame();
        }
        private void PauseGame() 
        {
            Time.timeScale = 0;
        }
        private void UnPauseGame()
        {
            Time.timeScale = 1;
        }
    }
}
