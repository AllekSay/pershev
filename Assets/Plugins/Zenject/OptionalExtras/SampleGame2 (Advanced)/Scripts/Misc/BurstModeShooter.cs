﻿using System;
using System.Collections;
using UnityEngine;

namespace Zenject.SpaceFighter
{
    public class BurstModeShooter : Shooter
    {
        private readonly AudioPlayer _audioPlayer;
        private readonly Settings _settings;
        private readonly Bullet.Factory _bulletFactory;
        private readonly CoroutinePorcessor _coroutinePorcessor;

        private Coroutine _shooting; 

        protected override Vector3 Position 
        {
            get
            {
                return _owner.position + (-_owner.transform.right * _settings.BulletOffsetDistance);
            }
        }

        public BurstModeShooter(
           CoroutinePorcessor coroutinePorcessor,
           Bullet.Factory bulletFactory,
           Settings settings,
           AudioPlayer audioPlayer,
           Rigidbody owner
           ) : base(owner)
        {
            _coroutinePorcessor = coroutinePorcessor;
            _audioPlayer = audioPlayer;
            _settings = settings;
            _bulletFactory = bulletFactory;
        }
        public override void Fire()
        {
            _shooting = _coroutinePorcessor.StartCoroutine(TripleShot());
        }

        public override void StopFire()
        {
            base.StopFire();
            _coroutinePorcessor.StopCoroutine(_shooting);
        }

        private IEnumerator TripleShot() 
        {
            CanShoot = false;
            for (int i = 0; i < _settings.BulletsAmount; i++) 
            {
                _audioPlayer.Play(_settings.Laser, _settings.LaserVolume);
                var angle = UnityEngine.Random.Range(
                        -Math.Abs(_settings.ScatterAngle),
                        Math.Abs(_settings.ScatterAngle)
                        ) * 
                        (1 - i % (_settings.BulletsAmount - 1));
                Rotate(angle);
                SpawnBullet(Position, Rotation);
                yield return new WaitForSeconds(_settings.BulletInterval);
            }
            
            CanShoot = true;

        }

        private void Rotate(int angle)
        {
            Rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }

        private void SpawnBullet(Vector3 position, Quaternion rotation)
        {
            var bullet = _bulletFactory.Create(
                _settings.BulletSpeed, _settings.BulletLifetime, BulletTypes.FromPlayer);

            bullet.transform.position = position;
            bullet.transform.rotation = rotation;
        }


        [Serializable]
        public class Settings
        {
            public AudioClip Laser;
            public float LaserVolume = 1.0f;
            public int BulletsAmount;
            public float BulletLifetime;
            public float BulletSpeed;
            public float BulletOffsetDistance;
            public float BulletInterval;
            public int ScatterAngle;

        }
    }
}
