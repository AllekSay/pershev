using System;
using UnityEngine.SceneManagement;

namespace Zenject.SpaceFighter
{
    public class GameRestartHandler : IInitializable, IDisposable
    {
        readonly SignalBus _signalBus;

        bool _isDelaying;
        float _delayStartTime;

        public GameRestartHandler(SignalBus signalBus)
        {
            _signalBus = signalBus;
        }

        public void Initialize()
        {
            _signalBus.Subscribe<GameEndedSignal>(OnGameEnded);
        }

        public void Dispose()
        {
            _signalBus.Unsubscribe<GameEndedSignal>(OnGameEnded);
        }

        void OnGameEnded()
        {
            StartNewGame();
        }
        private void StartNewGame()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

    }
}
