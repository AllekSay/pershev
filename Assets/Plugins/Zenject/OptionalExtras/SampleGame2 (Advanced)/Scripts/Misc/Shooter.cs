﻿using System;
using UnityEngine;

namespace Zenject.SpaceFighter
{
    public abstract class Shooter
    {
        protected Quaternion _turning;
        protected readonly Rigidbody _owner;
        protected virtual Vector3 Position
        {
            get
            {
                return _owner.position;
            }
        }

        protected virtual Quaternion Rotation 
        {
            get 
            {
                return _owner.rotation * _turning;
            }

            set 
            {
                _turning = value;
            }
        }

        public bool CanShoot { get; protected set; } = true;

        public Shooter(Rigidbody owner)
        {
            _owner = owner;
        }
        public abstract void Fire();
        public virtual void StopFire() { 
            CanShoot = true; 
        }
    }
}
