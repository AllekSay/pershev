﻿using UnityEngine;

public abstract class MenuAction : MonoBehaviour
{
    public abstract void Run();
}
