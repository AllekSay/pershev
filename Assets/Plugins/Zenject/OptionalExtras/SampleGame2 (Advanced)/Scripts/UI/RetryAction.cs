﻿namespace Zenject.SpaceFighter
{
    public class RetryAction : MenuAction
    {
        private SignalBus _signalBus;

        [Inject]
        public void Construct(SignalBus signalBus)
        {
            _signalBus = signalBus;
        }
        public override void Run()
        {
            _signalBus.Fire<GameEndedSignal>();
        }
    }
}
