﻿namespace Zenject.SpaceFighter
{
    public class ContinueAction : MenuAction
    {
        private SignalBus _signalBus;

        [Inject]
        public void Construct(SignalBus signalBus)
        {
            _signalBus = signalBus;
        }
        public override void Run()
        {
            _signalBus.Fire<GameResumedSignal>();
        }
    }
}