﻿using System;
using UnityEngine;

namespace Zenject.SpaceFighter
{
    public class ActivitiesController : IInitializable, IDisposable
    {
        private readonly Activities _activities;
        private readonly SignalBus _signalBus;
        public ActivitiesController(Activities activities, SignalBus signalBus)
        {
            _activities = activities;
            _signalBus = signalBus;
        }

        public void Initialize()
        {
            _signalBus.Subscribe<PlayerDiedSignal>(OnPlayerDied);
            _signalBus.Subscribe<GameResumedSignal>(OnGameResumed);
        }

        public void Dispose()
        {
            _signalBus.Unsubscribe<PlayerDiedSignal>(OnPlayerDied);
            _signalBus.Unsubscribe<GameResumedSignal>(OnGameResumed);
        }

        private void OnPlayerDied() 
        {
            _activities.GameOverActivity.SetActive(true);
        }
        private void OnGameResumed()
        {
            _activities.GameOverActivity.SetActive(false);
        }

        [Serializable]
        public class Activities
        {
            public GameObject GameOverActivity;
        }
    }
}