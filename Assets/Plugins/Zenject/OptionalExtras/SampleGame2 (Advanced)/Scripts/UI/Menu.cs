﻿using UnityEngine;

namespace Zenject.SpaceFighter
{
    public class Menu : MonoBehaviour
    {
        public void RunAction(MenuAction action)
        {
            action.Run();
        }
    }
}
