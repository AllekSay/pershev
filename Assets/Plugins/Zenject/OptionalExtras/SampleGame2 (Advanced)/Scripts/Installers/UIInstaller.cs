﻿namespace Zenject.SpaceFighter
{
    public class UIInstaller : MonoInstaller
    {
        public ActivitiesController.Activities Activities;

        public override void InstallBindings()
        {
            Container.BindInstance(Activities);
            Container.BindInterfacesTo<ActivitiesController>().AsSingle();
        }
    }
}
